package com.example.card.entity;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.math.BigDecimal;

import java.time.LocalDateTime;

@Entity
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@Getter
@Setter
@Builder
@AllArgsConstructor
@Table(name = "card")

public class CardEntity {


    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    Integer id;

    @Column(name = "card_name")
    String cardName;

    @Column(name = "card_type", nullable = false)
    String cardType;

    @Column(name = "card_number", nullable = false)
    String cardNumber;

    @Column(name = "card_duration")
    Integer cardDuration;

    @Column(name = "card_price")
    BigDecimal cardPrice;

    @Column(name = "is_active")
    Boolean isActive;


    @CreationTimestamp
    @Column(name = "created_at", nullable = false)
    LocalDateTime createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at", nullable = true)
    LocalDateTime updatedAt;

 }
