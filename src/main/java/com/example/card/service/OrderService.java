package com.example.card.service;

import com.example.card.dto.CardOrderDto;
import com.example.card.dto.OrderDto;

public interface OrderService {
    CardOrderDto orderCard(OrderDto createCardOrderDto);
}
