package com.example.card.service.impl;

import com.example.card.client.UserMsClient;
import com.example.card.client.model.UserResponce;
import com.example.card.service.UserMsService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserMsServiceImpl implements UserMsService {
  // private final UserMsService userMsService;
   private final UserMsClient userMsClient;
    @Override
       public UserResponce getUserById(Long id) {
        return userMsClient.getUser(id);
    }
}
