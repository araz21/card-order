package com.example.card.service.impl;

import com.example.card.dto.CardDto;
import com.example.card.dto.CreateCardDto;
import com.example.card.dto.UpdateCardDto;
import com.example.card.entity.CardEntity;
import com.example.card.exception.CardNotFoundException;
import com.example.card.mapper.CardMapper;
import com.example.card.repository.CardRepository;
import com.example.card.service.CardService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CardServiceImpl implements CardService {

    private static final Logger log = LoggerFactory.getLogger(CardServiceImpl.class);
    private final CardRepository cardRepository;
private final CardMapper cardMapper;


    @Override
    public CardDto createCard(CreateCardDto createCardDto) {
        CardEntity cardEntity = cardMapper.toCardEntity(createCardDto);

        CardEntity cardEntityDb = cardRepository.save(cardEntity);
        return cardMapper.toCardDto(cardEntityDb);
    }

    @CacheEvict(value = {"cardId", "cardList"}, key = "#id", allEntries = true)
    @Override
    public CardDto updateCard(Long id, UpdateCardDto updateCardDto) {
        CardEntity cardEntity = cardRepository.findById(id)
                .orElseThrow(() -> new CardNotFoundException( "Cards not found " + id));
        CardEntity updateCardEntity  = cardMapper.toCardEntity(updateCardDto, cardEntity);
        CardEntity updatedEntity=  cardRepository.save(updateCardEntity);
        return  cardMapper.toCardDto(updatedEntity);
    }

    @Cacheable(value = "cardList")
    @Override
    public List<CardDto> getCardList() {
        log.info("getCardList started");
        return cardRepository.findAll()
                .stream()
                .map(cardMapper::toCardDto)
                .toList();
    }
                           //SPel expresion
    @Cacheable(value ="cardId", key = "#id")
    @Override
    public CardDto getCardById(Long id) {
        CardEntity cardEntity = cardRepository.findById(id)
                .orElseThrow(() -> new CardNotFoundException( "Cards not found " + id));
        return cardMapper.toCardDto(cardEntity);
    }
}
