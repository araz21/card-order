package com.example.card.service.impl;
import com.example.card.client.model.UserResponce;
import com.example.card.dto.CardDto;
import com.example.card.dto.CardOrderDto;
import com.example.card.dto.OrderDto;
import com.example.card.entity.OrderEntity;
import com.example.card.mapper.OrderMapper;
import com.example.card.repository.OrderRepository;
import com.example.card.service.CardService;
import com.example.card.service.OrderService;
import com.example.card.service.UserMsService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {
    private final CardService cardService;
    private final OrderRepository orderRepository;
    private final UserMsService userMsService;
    private final OrderMapper orderMapper;

    @Override
    public CardOrderDto orderCard( OrderDto orderDto) {
        //check card id
       CardDto cardDto = cardService.getCardById(orderDto.getCardId());

        //check

        // card username
       UserResponce user = userMsService.getUserById(orderDto.getCardId());

        //save
        OrderEntity orderEntity = OrderEntity.builder()
                .userId(user.getId())
                .userName(user.getUserName())
                .cardId(cardDto.getId().toString())
                .build();
        orderRepository.save(orderEntity);


         return orderMapper.toCardOrderDto(orderRepository.save(orderEntity));
    }
}
