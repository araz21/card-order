package com.example.card.service;

import com.example.card.dto.CardDto;
import com.example.card.dto.CreateCardDto;
import com.example.card.dto.UpdateCardDto;

import java.util.List;

public interface CardService {

   CardDto createCard(CreateCardDto createCardDto);


    CardDto updateCard(Long id, UpdateCardDto updateCardDto);

    List<CardDto> getCardList();

    CardDto getCardById(Long id);
}
