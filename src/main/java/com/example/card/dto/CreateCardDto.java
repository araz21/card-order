package com.example.card.dto;


import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.PositiveOrZero;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class CreateCardDto {

    private String cardName;

    private String cardType;

    private String cardNumber;

    private Integer cardDuration;

    private BigDecimal cardPrice;

    private Boolean isActive;
}
