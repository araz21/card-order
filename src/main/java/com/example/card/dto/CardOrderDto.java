package com.example.card.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class CardOrderDto {

    Long id;
    Long cardId;
    Long userId;
    String userName;

}
