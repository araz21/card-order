package com.example.card.exception;

public class UserClientException extends RuntimeException{

    public UserClientException(String message) {
      super(message);
      }

}
