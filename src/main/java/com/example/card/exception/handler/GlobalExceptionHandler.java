package com.example.card.exception.handler;

import com.example.card.exception.CardNotFoundException;
import com.example.card.exception.UserClientException;
import com.example.card.exception.model.ErrorResponce;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
 import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

//@RestControllerAdvice
@RequiredArgsConstructor
public class GlobalExceptionHandler {
    private final MessageSource messageSource;

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ErrorResponce handleMethodArgumentNotValidException(MethodArgumentNotValidException ex, HttpServletRequest request) {
        Map<String, String> errors = new HashMap<>();

        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return  ErrorResponce.builder()
                .errors(errors)
                .message(ex.getMessage())
                .timestamp(LocalDateTime.now())
                .path(request.getServletPath())
                .build();
    }


    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(UserClientException.class)
    public ErrorResponce handleUserClientException(UserClientException exception,
                                                   HttpServletRequest request,
                                                   Locale locale
                                                   ) {
        String messageCode = exception.getMessage();
        Object[] arr = {"other"};
       String translatedMessage = messageSource.getMessage(messageCode,arr, locale );
        return  ErrorResponce.builder()
                .message(translatedMessage)
                .timestamp(LocalDateTime.now())
                .path(request.getServletPath())
                .build();

    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(CardNotFoundException.class)
    public ErrorResponce handleCardNotFoundException(CardNotFoundException exception,
                                                     HttpServletRequest request) {
     return  ErrorResponce.builder()
                     .message(exception.getMessage())
                     .timestamp(LocalDateTime.now())
                     .path(request.getServletPath())
                     .build();

    }


    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(Exception.class)
    public ErrorResponce handleException(Exception exception,
                                                     HttpServletRequest request) {
        return  ErrorResponce.builder()
                .message("Something went wrong")
                .timestamp(LocalDateTime.now())
                .path(request.getServletPath())
                .build();

    }
}
