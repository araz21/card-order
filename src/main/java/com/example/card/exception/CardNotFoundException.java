package com.example.card.exception;

public class CardNotFoundException  extends RuntimeException{

    public CardNotFoundException(String message){
        super(message);
    }
}
