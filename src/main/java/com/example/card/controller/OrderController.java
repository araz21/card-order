package com.example.card.controller;

import com.example.card.dto.CardOrderDto;
import com.example.card.dto.OrderDto;
import com.example.card.service.OrderService;
import com.example.card.service.impl.OrderServiceImpl;
import jakarta.validation.Valid;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/order")
public class OrderController {


    private final OrderService orderService;

    @PostMapping
    public ResponseEntity<CardOrderDto> orderCard(@Valid  @RequestBody OrderDto   orderDto) {
        return ResponseEntity.ok(orderService.orderCard(orderDto));
    }

}
