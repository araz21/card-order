package com.example.card.mapper;


import com.example.card.dto.CardDto;
import com.example.card.dto.CreateCardDto;
import com.example.card.dto.UpdateCardDto;
import com.example.card.entity.CardEntity;
import org.mapstruct.*;


@Mapper(componentModel = MappingConstants.ComponentModel.SPRING,
        unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface CardMapper {

    @Mapping(target = "isActive", source = "isActive")
    CardEntity toCardEntity(CreateCardDto createCardDto);

    CardDto toCardDto(CardEntity cardEntity);

    CardEntity toCardEntity(UpdateCardDto updateCardDto,@MappingTarget CardEntity cardEntity);
}

