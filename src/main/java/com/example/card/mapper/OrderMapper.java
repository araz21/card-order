package com.example.card.mapper;


import com.example.card.dto.CardOrderDto;
import com.example.card.entity.OrderEntity;
import org.mapstruct.*;


@Mapper(componentModel = MappingConstants.ComponentModel.SPRING,
        unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface OrderMapper {

    CardOrderDto toCardDto(OrderEntity save);

    CardOrderDto toCardOrderDto(OrderEntity save);
}

