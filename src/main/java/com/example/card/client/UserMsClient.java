package com.example.card.client;

import com.example.card.client.config.UserClientConfiguration;
import com.example.card.client.model.UserResponce;
import com.example.card.dto.CreateUserDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(
        name = "user-ms-client",
        url = "${api.users.url}",
        configuration = UserClientConfiguration.class,
        path = "/user"
)
public interface UserMsClient {
    @GetMapping("/{id}")
    UserResponce getUser(@PathVariable("id") Long id);

    @PostMapping
    UserResponce createUser(@RequestBody CreateUserDto createUserDto);
}
